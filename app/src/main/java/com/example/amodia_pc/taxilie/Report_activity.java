package com.example.amodia_pc.taxilie;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by admin on 1/27/2016.
 */
public class Report_activity extends AppCompatActivity {

    //    private Button rd;
    private ImageView image;
    private TextView tx;
    private CheckBox check1, check2, check3, check4, check5, check6, check7,check8,check9,
                     check10, check11, check12,check13,check14, check15, check16;

        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.report_activity);

            image = (ImageView)findViewById(R.id.person_photo);
            tx = (TextView)findViewById(R.id.text);
            check1 = (CheckBox)findViewById(R.id.c1);
            check2 = (CheckBox)findViewById(R.id.c2);
            check3 = (CheckBox)findViewById(R.id.c3);
            check4 = (CheckBox)findViewById(R.id.c4);
            check5 = (CheckBox)findViewById(R.id.c5);
            check6 = (CheckBox)findViewById(R.id.c6);
            check7 = (CheckBox)findViewById(R.id.c7);
            check8 = (CheckBox)findViewById(R.id.c8);
            check9 = (CheckBox)findViewById(R.id.c9);
            check10 = (CheckBox)findViewById(R.id.c10);
            check11 = (CheckBox)findViewById(R.id.c11);
            check12 = (CheckBox)findViewById(R.id.c12);
            check13 = (CheckBox)findViewById(R.id.c13);
            check14 = (CheckBox)findViewById(R.id.c14);
            check15 = (CheckBox)findViewById(R.id.c15);
            check16 = (CheckBox)findViewById(R.id.c16);


/*
            String[] violations = {"Allowing another person to use drivers license.", "Arrogance/Discourtesy", "Broken taximeter seal"
                    ,"Driving in sleeveless shirt","Driving in slippers","Driving under influence of liquor",
                    "Driving under influence of drugs", "Driving while using cellular phone",
                    "Dribing without license","Fake / Altered taximeter seal","Fast / Defect / Non-oper / Tampered taxi meter",
                    "Flagged up meter", "Loading / Unloading in prohibited zone", "No name of owner / Operator",
                    "Overcharging","Overtaking at no overtaking zone","Overtaking at an intersection","Overtaking bet men working / Caution signs",
                    "Overtaking upon a curve", "Plates different from body number", "Reckless driving",
                    "Refusal to convey passenger to destination"};
            ListAdapter buckysAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, violations);
            ListView buckysListview = (ListView)findViewById(R.id.listView);
            buckysListview.setAdapter(buckysAdapter);

            rd = (Button)findViewById(R.id.button);
*/
            // Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
            //setSupportActionBar(toolbar);

          /*  FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
            fab.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();
                }
            });
*/
        }

}
