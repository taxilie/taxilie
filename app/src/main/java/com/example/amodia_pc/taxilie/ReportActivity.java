package com.example.amodia_pc.taxilie;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Toast;

public class ReportActivity extends Activity {

    private Button otherReport;
    final Context context = this;
    private RadioButton oneR;
    private RadioButton twoR;
    private RadioButton threeR;
    private RadioButton fiveR;
    private RadioButton sixR;
    private RadioButton sevenR;
    private RadioButton eightR;
    private RadioButton nineR;
    private RadioButton tenR;
    private RadioButton elevenR;
    private RadioButton twelveR;
    private RadioButton thirteenR;
    private RadioButton fourteenR;
    private RadioButton fifteenR;
    private RadioButton sixteenR;
    private RadioButton seventeenR;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_report);

        otherReport = (Button)findViewById(R.id.btnOtherRep);
        oneR= (RadioButton)findViewById(R.id.one);
        twoR= (RadioButton)findViewById(R.id.two);
        threeR= (RadioButton)findViewById(R.id.three);
        fiveR= (RadioButton)findViewById(R.id.five);
        sixR= (RadioButton)findViewById(R.id.six);
        sevenR= (RadioButton)findViewById(R.id.seven);
        eightR= (RadioButton)findViewById(R.id.eight);
        nineR= (RadioButton)findViewById(R.id.nine);
        tenR= (RadioButton)findViewById(R.id.ten);
        elevenR= (RadioButton)findViewById(R.id.eleven);
        twelveR= (RadioButton)findViewById(R.id.twelve);
        thirteenR= (RadioButton)findViewById(R.id.thirteen);
        fourteenR= (RadioButton)findViewById(R.id.fourteen);
        fifteenR= (RadioButton)findViewById(R.id.fifteen);
        sixteenR= (RadioButton)findViewById(R.id.sixteen);
        seventeenR= (RadioButton)findViewById(R.id.seventeen);



        otherReport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder alert = new AlertDialog.Builder(context);
                alert.setTitle("Specify your other reports");
                final EditText input = new EditText(context);
                alert.setView(input);

                alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        //You will get as string input data in this variable.
                        // here we convert the input to a string and show in a toast.
                        String srt = input.getEditableText().toString();
                        Toast.makeText(context, srt, Toast.LENGTH_LONG).show();
                    } // End of onClick(DialogInterface dialog, int whichButton)
                }); //End of alert.setPositiveButton
                alert.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        // Canceled.
                        dialog.cancel();
                    }
                }); //End of alert.setNegativeButton
                AlertDialog alertDialog = alert.create();
                alertDialog.show();
       /* Alert Dialog Code End*/
            }// End of onClick(View v)



        });
    }

}
